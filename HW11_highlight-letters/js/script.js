document.addEventListener('keypress', (event) => {
    
    const btn = document.querySelectorAll('.btn')
    
    btn.forEach(element => {
        if (element.dataset.code === event.code) {
            element.classList.add('btn-blue')
        } else if (element.dataset.code !== event.code) {
            element.classList.remove('btn-blue')
        }
    })
})
