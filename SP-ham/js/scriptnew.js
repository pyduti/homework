const slideElements = {
    
}

//для создание доп галлереи
function createMorePicture() {
    const box = document.querySelector('.work-pic-box');
    const workGallary = [{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/lp/landing-page1.jpg',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/lp/landing-page5.jpg',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/lp/landing-page7.jpg',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/wp/wordpress1.jpg',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/All4.png',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/wp/wordpress5.jpg',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/All6.png',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/All7.png',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/wp/wordpress10.jpg',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/All9.png',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/All10.png',
    alt: 'IMG'
},
{
    tag: 'img',
    className: 'work-pic',
    src: './img/work/All.png',
    alt: 'IMG'
}];

for (let value of workGallary) {
    let workImg = document.createElement(value.tag);
    workImg.className = value.className;
    workImg.src = value.src;
    workImg.alt = value.alt;
    box.insertAdjacentElement('beforeend', workImg); 
}

}
//для али
function createAboutHasanAli() {
    const sayAbout = document.querySelector('.sayAbout-slider-wrapper');
    const workGallary = [{
        tag: 'img',
        className: 'slider-img-show',
        src: './img/about/about1.png'
    },    
    {
        tag: 'span',
        className: 'slider-prof',
        text: 'UX Designer',
    },
    {
        tag: 'span',
        className: 'slider-name',
        text: 'Hasan Ali'
    },
    {
        tag: 'p',
        className: 'slider-comment',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, 
        non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. 
        Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.lorem
         Morbi pulvinar odio eget aliquam facilisis.`
    }];
    
    for (let value of workGallary) {
            let workImg = document.createElement(value.tag);
            workImg.className = value.className;
            workImg.textContent = value.text;
            workImg.src = value.src
            sayAbout.insertAdjacentElement('afterbegin', workImg);
      }
}
//для д
function createAboutBlondGirl() {
    const sayAbout = document.querySelector('.sayAbout-slider-wrapper');
    const workGallary = [{
        tag: 'img',
        className: 'slider-img-show',
        src: './img/about/about.png'
    },    
    {
        tag: 'span',
        className: 'slider-prof',
        text: 'UXX Designer',
    },
    {
        tag: 'span',
        className: 'slider-name',
        text: 'Blond Girl'
    },
    {
        tag: 'p',
        className: 'slider-comment',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, 
        non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. 
        Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.lorem
         Morbi pulvinar odio`
    }];
    
    for (let value of workGallary) {
            let workImg = document.createElement(value.tag);
            workImg.className = value.className;
            workImg.textContent = value.text;
            workImg.src = value.src;
            sayAbout.insertAdjacentElement('afterbegin', workImg);
      }
}
//для д
function createAboutBrownGirl() {
    const sayAbout = document.querySelector('.sayAbout-slider-wrapper');
    const workGallary = [{
        tag: 'img',
        className: 'slider-img-show',
        src: './img/about/about3.png'
    },{
        tag: 'span',
        className: 'slider-prof',
        text: 'UUX Designer',
    },
    {
        tag: 'span',
        className: 'slider-name',
        text: 'Brown Girl'
    },
    {
        tag: 'p',
        className: 'slider-comment',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, 
        non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. 
        Tempus ultricies luctus, quam dui laoreet sem.`
    }];
    
    for (let value of workGallary) {
            let workImg = document.createElement(value.tag);
            workImg.className = value.className;
            workImg.textContent = value.text;
            workImg.src= value.src;
            sayAbout.insertAdjacentElement('afterbegin', workImg);
      }
}
//для м
function createAboutMan() {
    const sayAbout = document.querySelector('.sayAbout-slider-wrapper');
    const workGallary = [{
        tag: 'img',
        className: 'slider-img-show',
        src: './img/about/about2.png'
    },
    {
        tag: 'span',
        className: 'slider-prof',
        text: 'UI Designer',
    },
    {
        tag: 'span',
        className: 'slider-name',
        text: 'Man in black'
    },
    {
        tag: 'p',
        className: 'slider-comment',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus.`
    }];
    
    for (let value of workGallary) {
            let workImg = document.createElement(value.tag);
            workImg.className = value.className;
            workImg.textContent = value.text;
            workImg.src = value.src;
            sayAbout.insertAdjacentElement('afterbegin', workImg);
      }
}