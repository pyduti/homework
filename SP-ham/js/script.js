//--------------------------------------------Our services
$('.service-menu-item').on('click', function() {
    $('.service-menu-item.mint').removeClass('mint');
    $(this).addClass('mint');
    $('.service-box').addClass('invisible')
    $('.service-box')
        .eq(this.dataset.switch)
            .removeClass('invisible');
}); //Для переключателя Our services
//--------------------------------------------Our services
const elementsHTML = {
    all: [{
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All1.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All2.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All3.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All4.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All5.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All6.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All7.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All8.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All9.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All10.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All.png',
        alt: 'IMG'
    }],
    wordpress: [{
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress1.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress2.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress3.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress4.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress5.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress6.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress7.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress8.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress9.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress10.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All10.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All.png',
        alt: 'IMG'
    }],
    webdesign: [{
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wd/web-design1.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wd/web-design2.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wd/web-design3.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wd/web-design4.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wd/web-design5.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wd/web-design6.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress7.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress8.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress9.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress10.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All10.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All.png',
        alt: 'IMG'
    }],
    landingpages: [{
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page1.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page2.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page3.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page4.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page5.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page6.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page7.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress8.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress9.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress10.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All10.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All.png',
        alt: 'IMG'
    }],
    graphicdesign: [{
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design1.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design2.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design3.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design4.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design5.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design6.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design7.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design8.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design9.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design10.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design11.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/gd/graphic-design12.jpg',
        alt: 'IMG'
    }],
    more: [{
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page1.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page5.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/lp/landing-page7.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress1.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All4.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress5.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All6.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All7.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/wp/wordpress10.jpg',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All9.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All10.png',
        alt: 'IMG'
    },
    {
        tag: 'img',
        className: 'work-pic',
        src: './img/work/All.png',
        alt: 'IMG'
    }],
}//.work-menu-img obj
function createHTML(str) {
    for (let value in elementsHTML) {
        if (str === value) {
            obj = elementsHTML[value];
        }
    }
    const box = document.querySelector('.work-pic-box');
        for (let value of obj) {
        let workImg = document.createElement(value.tag);
        workImg.className = value.className;
        workImg.src = value.src;
        workImg.alt = value.alt;
        box.insertAdjacentElement('beforeend', workImg); 
    }
}//.work-menu-img creater
//--------------------------------------------Event WorkMenu
$('.work-menu-item').on('click', function() {
    $('.work-pic').remove();
    createHTML(this.dataset.name)
})
$('[data-name="all"]').trigger('click')//Переключатель .work-menu-item
$('.work-btn').on('click', function() {
    let img = $('<progress>').attr({'class' : 'preload'})
    $('.work-pic-box').after(img);
    setTimeout(() => {
        img.remove();
            createHTML('more');//eще work-piс
        $(this).remove();
    }, 1500);
}) //Для preload + 12img+
//----------------------------------------------Event WorkMenu
//----------------------------------------------Slider
const slideElements = {
    'first employee': [{
        tag: 'img',
        className: 'slider-img-show',
        src: './img/about/about1.png'
    },    
    {
        tag: 'span',
        className: 'slider-prof',
        text: 'UX Designer',
    },
    {
        tag: 'span',
        className: 'slider-name',
        text: 'Hasan Ali'
    },
    {
        tag: 'p',
        className: 'slider-comment',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, 
        non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. 
        Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.lorem
         Morbi pulvinar odio eget aliquam facilisis.`
    }],
    'second employee': [{
        tag: 'img',
        className: 'slider-img-show',
        src: './img/about/about.png'
    },    
    {
        tag: 'span',
        className: 'slider-prof',
        text: 'UXX Designer',
    },
    {
        tag: 'span',
        className: 'slider-name',
        text: 'Hasan Alisa'
    },
    {
        tag: 'p',
        className: 'slider-comment',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, 
        non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. 
        Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.lorem
         Morbi pulvinar odio`
    }],
    'third employee': [{
        tag: 'img',
        className: 'slider-img-show',
        src: './img/about/about3.png'
    },{
        tag: 'span',
        className: 'slider-prof',
        text: 'UUX Designer',
    },
    {
        tag: 'span',
        className: 'slider-name',
        text: 'Hasan Anya'
    },
    {
        tag: 'p',
        className: 'slider-comment',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, 
        non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. 
        Tempus ultricies luctus, quam dui laoreet sem.`
    }],
    'fourth employee': [{
        tag: 'img',
        className: 'slider-img-show',
        src: './img/about/about2.png'
    },
    {
        tag: 'span',
        className: 'slider-prof',
        text: 'UI Designer',
    },
    {
        tag: 'span',
        className: 'slider-name',
        text: 'Hasan Aslan'
    },
    {
        tag: 'p',
        className: 'slider-comment',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus.`
    }]
}//.slide obj
function createSlideEl(str) {
    for (let value in slideElements) {
        if ($('.slider-img-item.focus')[0].dataset.name === value) {
            obj = slideElements[value];
        }
    }
    const sayAbout = document.querySelector('.sayAbout-slider-wrapper');
    for (let value of obj) {
        let slideContent = document.createElement(value.tag);
        slideContent.className = value.className;
        slideContent.textContent = value.text;
        slideContent.src = value.src
        sayAbout.insertAdjacentElement('afterbegin', slideContent);
  }
}//.slide-emp-creater
function removeSlideElements () {
    $('.slider-img-show').remove();
    $('.slider-comment').remove();
    $('.slider-name').remove();
    $('.slider-prof').remove();
}
$('.btn-move.right').on('click', function() {
    if ((Number($('.slider-img-item.focus')[0].dataset.switch) + 1) !== Number($('.slider-img-item').length)) {
        let number = Number($('.slider-img-item.focus')[0].dataset.switch) + 1;
        $('.slider-img-item').removeClass('focus')
        $('.slider-img-item')
            .eq(number)
                .addClass('focus')
                    removeSlideElements();
                        createSlideEl($('.slider-img-item.focus')[0].dataset.name);
    } else {
        $('.slider-img-item').removeClass('focus');
        $('.slider-img-item')
            .eq(0).addClass('focus');
                removeSlideElements();
                    createSlideEl($('.slider-img-item.focus')[0].dataset.name);
    }
})//Для правой кнопки
$('.btn-move.left').on('click', function() {
    if ((Number($('.slider-img-item.focus')[0].dataset.switch) - 1) >= 0) {
        let number = Number($('.slider-img-item.focus')[0].dataset.switch) - 1;
        $('.slider-img-item').removeClass('focus')
        $('.slider-img-item')
            .eq(number)
                .addClass('focus')
                    removeSlideElements();
                        createSlideEl($('.slider-img-item.focus')[0].dataset.name);
    } else {
        $('.slider-img-item').removeClass('focus');
        $('.slider-img-item')
            .eq(Number($('.slider-img-item').length) - 1)
                .addClass('focus');
                    removeSlideElements();
                        createSlideEl($('.slider-img-item.focus')[0].dataset.name);
    }
})//Для левой кнопки
$('.slider-img-item').on('click', function() {
    $('.slider-img-item').removeClass('focus')
    $('.slider-img-item')
        .eq(this.dataset.switch)
            .addClass('focus')
                removeSlideElements();
                    createSlideEl($('.slider-img-item.focus')[0].dataset.name);
})//Для клика по картинке
$('.slider-img-item.focus').trigger('click');
//----------------------------------------------Slider