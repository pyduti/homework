function calculate (firstUserNumber, secondUserNumber, userSign) {
    switch (userSign) {
        case `/`:
            return firstUserNumber / secondUserNumber;
        case `*`: 
            return firstUserNumber * secondUserNumber;
        case `-`: 
            return firstUserNumber - secondUserNumber;
        case `+`: 
            return firstUserNumber + secondUserNumber;
    }
}

let firstUserNumber = +prompt('Enter first number');
let secondUserNumber = +prompt('Enter second number');
let userSign = prompt('Enter sign: / * - +');

while (isNaN(firstUserNumber) || isNaN(secondUserNumber) || !isNaN(userSign)) {
    firstUserNumber = +prompt('Enter first number');
    secondUserNumber = +prompt('Enter second number');
    userSign = prompt(`Enter sign: / * - +`);
}

console.log(calculate(firstUserNumber, secondUserNumber, userSign));