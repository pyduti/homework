document.querySelectorAll('.password-form').forEach(function() {
    this.addEventListener('click', function(event) {
        if(event.target.className === 'fas fa-eye icon-password'){
            event.target.className = 'fas fa-eye-slash icon-password'
            event.target.previousElementSibling.setAttribute('type', 'word');
        } else if (event.target.className === 'fas fa-eye-slash icon-password'){
            event.target.className = 'fas fa-eye icon-password'
            event.target.previousElementSibling.setAttribute('type', 'password');
        }
    })
})

document.querySelector('.password-form').addEventListener('submit', function(event) {
    event.preventDefault();
    if (document.querySelector('.uncorrect-pwd')) {
        document.querySelector('.uncorrect-pwd').remove();
    }
    if (document.querySelector('.pwd').value !== document.querySelector('.sub-pwd').value) {
        document.querySelector('#sub').insertAdjacentHTML('afterend', `<span class = 'uncorrect-pwd'>Нужно ввести одинаковые значения</span>`)
    } else {
        alert('You are welcome');
    }
})
