const tabs = document.querySelector('#tabs');
const tabsContent = document.querySelectorAll('.tabs-content-title');
const tabsTitle = document.querySelectorAll('.tabs-title');

tabsContent.forEach((eContent) => {
    tabs.addEventListener('click', eTabs => {
        eTabs.preventDefault();

        let tabsTitle = Number(eTabs.target.dataset.active);
        let content = Number(eContent.getAttribute('data-content'));
        
        if ( tabsTitle === content ) {
            clear();
            eTabs.target.classList.add('active');
            eContent.classList.add('visible')
        } else {
            eContent.classList.remove('visible')
        }
    
    })
})

function clear() {
tabsTitle.forEach((eTitle) => {
    eTitle.classList.remove('active');
})

}
