function findMultiples() {
    let userNumber = prompt('Enter number:');
    
    while (userNumber < 0 || userNumber == null || isNaN(userNumber) ) {
        userNumber = prompt('Enter number:');
    }

    if (userNumber < 5) {
        console.log('Sorry, no numbers');
    }

    for (let i = 1; i <= userNumber; i++) {
            if (i % 5 == 0) {
                console.log(i);
            }
    }
}

function prime() {
    let m = prompt('Enter first number:');
    let n = prompt('Enter second number:');

    while (m > n || m < 0 || isNaN(m) || isNaN(n) || m == null || n == null) {
        m = prompt('Enter first number:');
        n = prompt('Enter second number:');
    }

    for (;m <= n; m++) {
        if (m <= 1) {
            continue;    
        } else if (m == 2 || m == 3 || m == 5 || m == 7) {
            console.log(m);
        } else if (m % 2 == 0 || m % 3 == 0 || m % 5 == 0 || m % 7 == 0) {
            continue;
        } else {
            console.log(m);
        }
    }
}


let userChoise = +prompt('Enter you choise: 1 - Числа кратные пяти; 2 - Простое число;');

if (userChoise == 1) {
    
    findMultiples();

} else if (userChoise == 2) {

    prime();

} else {

    console.log('Try next time!');

}
