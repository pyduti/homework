const switchTheam = document.head.lastElementChild;
switchTheam.href = localStorage.getItem('href');

document.addEventListener ('click', event => {
    if (event.target.dataset.switch === 'black') {
        event.target.dataset.switch = `white`;
        switchTheam.href = `./css/style-bg-white.css`;
        localStorage.setItem('href', switchTheam.href);
    } else {
        event.target.dataset.switch = `black`;
        switchTheam.href = `./css/style-bg-black.css`;
        localStorage.setItem('href', switchTheam.href);
    }
})
