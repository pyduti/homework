function filterBy(userArray,userType) {
    const filterArray = userArray.filter((el) => userType !== typeof(el)) 
    return filterArray;
}

const userArray = ['hello', 'world', 23, '23', null, ['hello', 'world', 23, '23', null], true, false, Symbol("foo"), {}, undefined, 23n, function(){}];
const userType = prompt('Enter some type:');

const newArray = filterBy(userArray,userType);
console.log(newArray);
