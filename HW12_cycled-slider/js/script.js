let isPaused = false; //Flag для паузы
let imgIndex = 0; //Index для картинки

function createAnimation() {
    setInterval(() => {
    const imageToShow = document.querySelectorAll('.image-to-show');
    if (!isPaused) {
        if (imgIndex === 0) {
            imageToShow[imgIndex++].classList.add('visible');
        } else if (imgIndex < imageToShow.length) {
            imageToShow[imgIndex++].classList.add('visible');
            imageToShow[imgIndex-2].classList.remove('visible');
        } else {
            imageToShow[imgIndex - 1].classList.remove('visible')
            imgIndex -= imageToShow.length - 1;//Убрал задержку в 3 секунды
            imageToShow[0].classList.add('visible');//Убрал задержку в 3 секунды
        }
    }
    }, 3000)
}

const buttonWrapper = document.querySelector('.button-wrapper')

buttonWrapper.addEventListener('click', (event) => {
    event.preventDefault();

    if ( event.target.dataset.stop ) {
        isPaused = Boolean(event.target.dataset.stop);
    } else if ( event.target.dataset.resume ) {
        isPaused = Boolean(event.target.dataset.reset);
    }
})

createAnimation()