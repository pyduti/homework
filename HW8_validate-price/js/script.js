const elements = [{
    tag: 'input',
    className: 'input-Price',
    type: 'number'
},
{
    tag: `span`,
    className: `uncorrect-Price`,
    type: 'number',
    textContent: `Please enter correct price!`
}];

document.body.insertAdjacentHTML('afterbegin', `<div class = wrapper><div class = price-box></div></div>`);
const priceBox = document.querySelector('.price-box');

function createHTMLElement(elements) {
    for (let i=0; i < elements.length; i++) {
        let htmlElement = document.createElement(elements[i].tag);
        htmlElement.className = elements[i].className;
        htmlElement.type = elements[i].type;
        htmlElement.textContent = elements[i].textContent;
        priceBox.insertAdjacentElement('beforeend', htmlElement);
        
    }
}

createHTMLElement(elements);
const price = document.querySelector('.input-Price');
const unCorrectPrice = document.querySelector('.uncorrect-Price');
const createSpan = () => {
    const span = document.querySelector('.correct-Price')
    if (!span) {
        return price.insertAdjacentHTML('beforebegin', `<span class = 'correct-Price'></span>`);
    } else { 
        return span.insertAdjacentHTML('beforebegin', `<span class = 'correct-Price'></span>`);
    }
}
const clearSpan = () => {
    const deletElement = document.querySelector('.correct-Price')
    deletElement.remove();
}

price.insertAdjacentText('beforebegin', 'Price $:');

price.addEventListener('focus', () => {
    price.classList.add('input-green');
    unCorrectPrice.classList.remove('UnCorrectValue');
    price.classList.remove('input-red');
});

price.addEventListener('blur', () => {
    createSpan();
    const correctPrice = document.querySelector('.correct-Price')
    price.classList.remove('input-green');
    if (price.value > 0) {
        correctPrice.textContent = `Текущая цена: ${price.value}`;
        price.classList.add('input-backGreen');
        correctPrice.classList.add('correctValue');
        
        const correctValue = document.querySelector('.correctValue');

        const close = document.createElement('span');
        close.className = 'close';
        correctValue.append(close);
        const closeElement = document.querySelector('.close')
        
        closeElement.onclick = () => {
            price.value = '';
            clearSpan();
            price.classList.remove('input-backGreen');
        }
    } else {
        price.classList.add('input-red');
        unCorrectPrice.classList.add('UnCorrectValue');
    }
});
