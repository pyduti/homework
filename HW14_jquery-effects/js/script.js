$(document).on('click', '.nav-menu-item_href', function(e) {
    e.preventDefault();
   $('html, body').animate({
       scrollTop: $($(this).attr('href')).offset().top
   }, 3000)
})

$(document).on("click", ".goTop", function(e) {
    e.preventDefault();
    $('body, html').animate({scrollTop: 0}, 3000);
});

($(document).scroll(function(e) {
    e.preventDefault();
    if ($(window)[0].screen.height <= $(document).scrollTop()) {
        $('.goTop').fadeIn();
    }  else {
        $('.goTop').fadeOut();
    }
    // if ($(document).scrollTop() > 0) {
    //     $('.goTop').fadeIn();
    // } else {
    //     $('.goTop').fadeOut();
    }))  
$('.off').on('click', function(e) {
    e.preventDefault();
    $(`.${this.previousElementSibling.className}`).slideToggle('slow');
})
